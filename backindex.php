<!DOCTYPE html>
<html lang="en">
  <head itemscope itemtype="http://schema.org/WebSite">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Purnima Gurung - Software developer</title>
    <meta name="description" content="Purnima Gurung is an accomplished web designer & front-end developer with extensive experience building websites using HTML5, CSS3 and various web scripting technologies, web standards and project management.She is an enthusiastic web professional motivated by challenging projects and deadlines.">
    <meta name="keywords" content="Front end Design, design, Front end developer, Designer, Design, HTML,CSS, Nepal, Purnima, Gurung,Purnima Gurung">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-117900214-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      
      gtag('config', 'UA-117900214-1');
    </script>
    <link rel="icon" type="image/png" sizes="16x16" href="images/mylogo.png" alt="puru">

    <link href="css/aos.css" rel="stylesheet" type="text/css" media="all" />
    <!-- //animation effects-css-->
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/customize.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css"/>
    <!-- font-awesome icons -->
    <!--//Custom Theme files-->
    <!-- web-fonts -->
    <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Arsenal:400,400i,700,700i" rel="stylesheet">
    <!-- //web-fonts -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body itemscope itemtype="http://schema.org/WebPage">
    <!--banner-->
    <div id="home" class="banner">
      <div class="banner-info">
        <div class="container">
          <div class="col-md-4 header-left"  data-aos="flip-right">
            <img src="images/4.PNG" alt=""/>
          </div>
          <div class="col-md-8 header-right">
           <h2 class="typewrite" data-period="2000" data-type='[ "Namaste", "Hello", "Konnichiwa", "Halo" ]'><span class="wrap"></h2>
            <h1>I'm Purnima Gurung</h1>
            <h6>Software Developer</h6>
            <ul class="address">
              <li>
                <ul class="address-text">
                  <li><b>D.O.B</b></li>
                  <li>August 25</li>
                </ul>
              </li>
              <li>
                <ul class="address-text">
                  <li><b>Occupation </b></li>
                  <li>Frontend Developer</li>
                </ul>
                </li>
              <li>
                <ul class="address-text">
                  <li><b>ADDRESS </b></li>
                  <li> Kathmandu , Nepal</li>
                </ul>
              </li>
              <li>
                <ul class="address-text">
                  <li><b>E-MAIL </b></li>
                  <li><a href="#">grg.puru@gmail.com</a></li>
                </ul>
              </li>
              <li>
                <ul class="address-text">
                  <li><b>Skype ID </b></li>
                  <li> anzu.gurung1</li>
                </ul>
                </li>
              <li>
              <li>
                <ul class="address-text">
                  <li><b>WEBSITE </b></li>
                  <li><a href="/">purnimagurung.com.np</a></li>
                </ul>
              </li>
            </ul>
          </div>
          <div class="clearfix"> </div>
          
        </div>
      </div>
    </div>
    <!--//banner-->
    <!--top-nav-->
    <div class="top-nav wow"  data-spy="affix" data-offset-top="197" itemscope itemtype="http://schema.org/SiteNavigationElement">
      <div class="container">
        <div class="navbar-header logo">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          Menu
          </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <div class="menu">
            <ul class="nav navbar">
              <li><a href="#about" class="scroll">About</a></li>
              <li><a href="#education" class="scroll">Education</a></li>
              <li><a href="#experience" class="scroll">Experience</a></li>
              <li><a href="#projects" class="scroll">My Projects</a></li>
              <li><a href="#volunteers" class="scroll">Volunteer</a></li>
              <li><a href="#contact" class="scroll">Contact</a></li>
            </ul>
            <div class="clearfix"> </div>
          </div>
        </div>
      </div>
    </div>
    <!--//top-nav-->
    <!-- about me -->
    <div class="about-section" id="about">
      <div class="container">
        <div class="row">
          <div class="col-md-7 col-sm-6">
            <div class="about-title clearfix">
              <h2 data-aos="zoom-in">About <span>Me</span></h2>
              <h3>Software Developer : +5 years of coding experience</h3>
              <p class="about-paddingB">Accomplished web designer & front-end developer with extensive experience building websites, HTML5, CSS3 and various web scripting technologies, web standards and project management.Enthusiastic web professional motivated by challenging projects and deadlines.</p>
              <a href="https://drive.google.com/file/d/1A6hhFbTGheZc6QQUQFk0uWIpULPKOcHf/view?usp=sharing" target="_blank" class="hireme about-link">My Resume</a>
              <ul class="social-links">
              	<li data-aos="flip-right"><a href="https://www.linkedin.com/in/purnima-gurung-946219105/" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                <li data-aos="flip-right"><a href="https://twitter.com/grg_puru" target="_blank"><i class="fa fa-twitter"></i></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-5 col-sm-6">
            <div class="about-img" data-aos="flip-right" >
              <img src="images/5.jpg" alt="puru" class="img-responsive">
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- skills -->
    <div class="skill-section">
      <div class="container">
        <div class="col-md-5 skill-">
          <h3 data-aos="zoom-in" class="skill-title">skills</h3>
          <p>Ncididunt ut labore data-aos="zoom-in"et t enim ad minim.cididunt ut labore et t enim ad minim.Ncididunt ut labore et t enim ad minim labore
            et t enim ad minim.Ncididunt ut labore et t enim a.
          </p>
        </div>
        <div class="col-md-7 skills-right">
          <div class="vertical-skills  pull-right xs-center" data-aos="zoom-in">
            <ul class="list-inline">
              <li>
                <div class="skill" style="height:100%; background:#ff9d0d;"><span class="value">100%</span></div>
                <span class="title">HTML</span>
              </li>
              <li>
                <div class="skill" style="height:100%; background:#03a9f4;"><span class="value">100%</span></div>
                <span class="title">CSS</span>
              </li>
              <li>
                <div class="skill" style="height:100%; background:#b32eca;"><span class="value">100%</span></div>
                <span class="title">Bootstrap</span>
              </li>
              <li>
                <div class="skill" style="height:75%; background:#009688;"><span class="value">75%</span></div>
                <span class="title">Photoshop</span>
              </li>
              <li>
                <div class="skill" style="height:66%; background:#6361f0;"><span class="value">55%</span></div>
                <span class="title">JS</span>
              </li>
            </ul>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
    <!--education-->
    <div class="education-section" id="education">
      <div class="container">
        <div class="education-title">
          <h2 data-aos="zoom-in">About <span>Education</span></h2>
          <div class="col-md-6">
            <div data-aos="zoom-in" class="blockquote-box blockquote-primary clearfix" style="display: block;">
              <div class="square pull-left">
                <i class="fa fa-calendar"></i> .. - 2008
              </div>
              <h3>Lumbini Boarding School</h3>
              <p class="text-justify box-p">
                Primary School. It was my childhood. Students around 1000 and staff around 40. I have studied from grade 1 to 10.
              </p>
              <address class="text-center"><strong>Kawasoti, Nawalparasi , Nepal</strong></address>
            </div>
          </div>
          <div class="col-md-6">
            <div data-aos="zoom-in" class="blockquote-box blockquote-success clearfix" style="display: block;">
              <div class="square pull-left">
                <i class="fa fa-calendar"></i> 2011
              </div>
              <h3>oxford Higher Secondary School</h3>
              <p class="text-justify box-p">
                Secondary School. Principle was Er. Hari Bhandari. I was very lucky to study under his control.
              </p>
              <address class="text-center"><strong>Gaidakot, Nawalparasi-2 , Nepal</strong></address>
            </div>
          </div>
          <div data-aos="zoom-in" class="col-md-6 col-md-offset-3">
            <div class="blockquote-box blockquote-danger clearfix" style="display: block;">
              <div class="square pull-left">
                <i class="fa fa-calendar"></i> 2015
              </div>
              <h3>New Summit College</h3>
              <p class="text-justify box-p">
                Graduate. It runs as an associate of KMC Educational Network. I have studied BSc.CSIT.
              </p>
              <address class="text-center"><strong>Old Baneshwor, Kathmandu, Nepal</strong></address>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--//education-->
    <!--work-experience-->
    <div class="experience-section" id="experience">
      <div class="work">
        <div class="container">
          <h3 class="title" data-aos="zoom-out">Work Experience</h3>
          <div class="work-timetable" data-aos="zoom-in">
            <div class="work-info">
              <div class="col-md-6 work-right work-right2">
                <h4>Since 2015 </h4>
              </div>
              <div class="col-md-6 work-left work-left2">
                <h5> <a href=""> Freelancer <span class="glyphicon glyphicon-briefcase"> </span></a></h5>
                <p>Working as Freelancer Frontend Developer since 2015. </p>
              </div>
              <div class="clearfix"> </div>
            </div>
            <div class="work-info">
              <div class="col-md-6 work-left">
                <h4>2016 - 2017 </h4>
              </div>
              <div class="col-md-6 work-right">
                <h5><a href="https://www.kullabs.com/" target="_blank"><span class="glyphicon glyphicon-briefcase"> </span>Kullabs.com</a></h5>
                <p>Worked as Frontend Developer for 1years 3 months.</p>
              </div>
              <div class="clearfix"> </div>
            </div>
            <div class="work-info">
              <div class="col-md-6 work-right work-right2">
                <h4>2017 - present </h4>
              </div>
              <div class="col-md-6 work-left work-left2">
                <h5><a href="https://www.edukum.com/" target="_blank"> Edukum.com <span class="glyphicon glyphicon-briefcase"> </span></a></h5>
                <p>Working as Frontend Developer since 2017. </p>
              </div>
              <div class="clearfix"> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--work-experience-->
   
    <!--work-portfolio-->
    <div class="portfolio-section" id="projects">
      <div class="container">
        <h3 data-aos="zoom-in" class="portfolio-title aos-init aos-animate">My <span>Portfolio</span></h3>
        <div class="row">
          <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="hovereffect">
                  <img class="img-responsive" alt="" src="images/portfolio/kullabs.jpg" alt="kullabs"/>
                  <div class="overlay">
                     <h2>Kul Techno Lab And Research Center (Kullabs)</h2>
                     <a class="info" href="#">Search for notes and study materials for every class, every subject</a>
                  </div>
              </div>
          </div>
          
          <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="hovereffect">
                  <img class="img-responsive" alt="" src="images/portfolio/edukum.png" alt="edukum" />
                  <div class="overlay">
                     <h2>Edukum Pvt.Ltd</h2>
                     <a class="info" href="#">Edukum is an educational website dedicated in ensuring students a complete freedom and ease in learning</a>
                  </div>
              </div>
          </div>
          <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="hovereffect">
                  <img class="img-responsive" alt="" src="images/portfolio/neef.png" alt="neef Nepal" />
                  <div class="overlay">
                     <h2>NEEF (Public Speaking)</h2>
                     <a class="info" href="#">Nepal Entrepreneurship Education Foundation (NEEF) is the first and only Public Speaking based Training Institution in Nepal with guaranteed performance.....</a>
                  </div>
              </div>
          </div>
          <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="hovereffect">
                 <img class="img-responsive" alt="" src="images/portfolio/kathmandusteel.png" alt="kathmandusteel"/>
                  <div class="overlay">
                     <h2>Kathmandu Steel Company</h2>
                     <a class="info" href="#">Kathmandu Steel, a new company in Napal, has introduced new brand of steel bars using Reidbar technology.This Company Produces Continuous Threaded TMT Reidbar Bar</a>
                  </div>
              </div>
          </div>
          <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="hovereffect">
                  <img class="img-responsive" alt="" src="images/portfolio/christmas.png" alt="christmas" />
                  <div class="overlay">
                     <h2>Christmas </h2>
                     <a class="info" href="#">PSD for Website</a>
                  </div>
              </div>
          </div>
          
          <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="hovereffect">
                  <img class="img-responsive" alt="" src="images/portfolio/operaconsultancy.png" alt="opera consultancy" />
                  <div class="overlay">
                     <h2>Opera Express Consultancy</h2>
                     <a class="info" href="#">Best Consultancy for Abroad Study which located at Putalisadk, Kathmandu....</a>
                  </div>
              </div>
          </div>
          <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="hovereffect">
                  <img class="img-responsive" alt="" src="images/portfolio/operaconsultancy.png" alt="opera consultancy" />
                  <div class="overlay">
                     <h2>Akashi Japanese Language School</h2>
                     <a class="info" href="#">Best Consultancy for Abroad Study which located at Bagbazaar, Kathmandu....</a>
                  </div>
              </div>
          </div>
          <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="hovereffect">
                  <img class="img-responsive" alt="" src="images/portfolio/pjc.png" alt="akashi Consultancy" />
                  <div class="overlay">
                     <h2>PJC Bridge Test ( Practical Japanese Communication Test)</h2>
                     <a class="info" href="#">PJC Bridge Examination is first online based exam introduced in Nepal</a>
                  </div>
              </div>
          </div>
        </div>
      </div>
    </div>
    <!-- ---- participation---- -->
     <div class="volunteer_works" id="volunteers">
      <div class="volunteer">
        <div class="container">
           <div class="col-md-7 col-md-offset-2">
              <h3 data-aos="zoom-out" class="volunteer-title" style="padding:0;"><span> <i class="fa fa-code"> </i></span> Participations <span>& Volunteering</span> <span> <i class="fa fa-code"> </i></span></h3>
              <ul class="participations">
                <li>Participated in
                  <strong>B.P memorial Health Foundation</strong> under EU/UNFPA Reproductive Health Initiative for Youth in Asia Program in Nepal -
                  <strong>2006</strong>
                </li>
                
                <li>Volunteering in
                  <strong>SUBisu CAN softech</strong> organized by
                  <a href="http://www.can.org.np/" target="_blank">Computer Association of Nepal</a>. -
                  <strong>2013</strong>
                </li>
                 <li>Participated in
                  <strong>6th CSIT Techno Share 2070 on "wordPress Startup"</strong> organized by
                  <a href="http://can.org.np" target="_blank">Computer Association of Nepal</a>. -
                  <strong>2013</strong>
                </li>

                <li>Volunteered in
                  <strong>CSIT Inter Quiz Competition</strong> organized by 
                   <a href="http://can.org.np" target="_blank">New Summit College</a>. -
                  <strong>2013</strong>
                </li>
                <li>Participated in
                  <strong> The computer software Survey in Kathmandu Valley</strong> organized by
                  <a href="http://www.can.org.np/" target="_blank">Computer Association of Nepal</a>. -
                  <strong>2014</strong>
                </li>
                  <li>Volunteering in
                  <strong>SUBisu CAN softech</strong> organized by
                  <a href="http://www.can.org.np/" target="_blank">Computer Association of Nepal</a>. -
                  <strong>2015</strong>
                </li>
                  <li>Volunteering in
                  <strong>Environment Cleaning Campaign </strong> organized by
                  <a href="http://kullabs.com/" target="_blank">Kul Techno Lab And Research Center</a>. with Kathmandu Metropolitan City Office -
                  <strong>2016</strong>
                </li>
              
              </ul>
           </div>
        <!--    <div class="col-md-6" style="border-left:4px solid #dcdcdc">
              <h3 data-aos="zoom-out" class="volunteer-title" style="padding:0;">Languages <span><i class="fa fa-code"> </i></span></h3>
               <div class="language-skills">
                <div class="skill">Nepali
                  <div class="icons pull-right">
                    <div style="width: 80%; overflow: hidden; height: 14px;" class="icons-red"></div>
                  </div>
                </div>
                <div class="skill">English
                  <div class="icons pull-right">
                    <div style="width: 60%; overflow: hidden; height: 14px;" class="icons-red"></div>
                  </div>
                </div>
                <div class="skill">Hindi
                  <div class="icons pull-right">
                    <div style="width: 60%; overflow: hidden; height: 14px;" class="icons-red"></div>
                  </div>
                </div>
              </div>
              <br>
              <h3 data-aos="zoom-out" class="volunteer-title" style="padding:0;"><span> <i class="fa fa-code"> </i></span> Hobbies <span></span></h3>
              <div class="Hobby">Travelling</div>
              <div class="Hobby">Birdwatching</div>
              <div class="Hobby">Music Lover</div>
              <div class="Hobby">Movies</div>
              <div class="Hobby">Novel-Books</div>

              
           </div> -->
        </div>
     </div>
    </div>
    <?php include 'contact.php';?>
    <div class="footer" itemscope itemtype="http://schema.org/WPFooter">
      <p>© 2019 Designer. All rights reserved | Design by <a href="https://purnimagurung.com.np/" target="_blank"> Purnima Gurung</a></p>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
     <!--PARTICLES ANIMATE JS-->
        <script src="js/particles.js"></script>  
        <script src="js/particles.app.js"></script> 
    <!-- animation effects-js files-->
    <script src="js/aos.js"></script><!-- //animation effects-js-->
    <script src="js/aos1.js"></script><!-- //animation effects-js-->
    <!-- animation effects-js files-->
    <!--start-smooth-scrolling-->
    <script type="text/javascript" src="js/move-top.js"></script>
    <script type="text/javascript" src="js/easing.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
    <script type="text/javascript">
       $(document).ready(function(){
          $(".fancybox").fancybox({
              openEffect: "none",
              closeEffect: "none"
          });
      });
    </script>
    <!-- here stars scrolling script -->

    <script type="text/javascript">
      $(document).ready(function() {
                    
        $().UItoTop({ easingType: 'easeOutQuart' });
                  
        });
    </script>
    <!-- //here ends scrolling script -->
    <!-- //here ends scrolling icon -->
    <script type="text/javascript">
      jQuery(document).ready(function($) {
        $(".scroll").click(function(event){   
          event.preventDefault();
          $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
      });
    </script>
       <script type="text/javascript">
      //made by vipul mirajkar thevipulm.appspot.com
var TxtType = function(el, toRotate, period) {
        this.toRotate = toRotate;
        this.el = el;
        this.loopNum = 0;
        this.period = parseInt(period, 10) || 2000;
        this.txt = '';
        this.tick();
        this.isDeleting = false;
    };

    TxtType.prototype.tick = function() {
        var i = this.loopNum % this.toRotate.length;
        var fullTxt = this.toRotate[i];

        if (this.isDeleting) {
        this.txt = fullTxt.substring(0, this.txt.length - 1);
        } else {
        this.txt = fullTxt.substring(0, this.txt.length + 1);
        }

        this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

        var that = this;
        var delta = 200 - Math.random() * 100;

        if (this.isDeleting) { delta /= 2; }

        if (!this.isDeleting && this.txt === fullTxt) {
        delta = this.period;
        this.isDeleting = true;
        } else if (this.isDeleting && this.txt === '') {
        this.isDeleting = false;
        this.loopNum++;
        delta = 500;
        }

        setTimeout(function() {
        that.tick();
        }, delta);
    };

    window.onload = function() {
        var elements = document.getElementsByClassName('typewrite');
        for (var i=0; i<elements.length; i++) {
            var toRotate = elements[i].getAttribute('data-type');
            var period = elements[i].getAttribute('data-period');
            if (toRotate) {
              new TxtType(elements[i], JSON.parse(toRotate), period);
            }
        }
        // INJECT CSS
        var css = document.createElement("style");
        css.type = "text/css";
        css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
        document.body.appendChild(css);
    };
    </script>
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

    <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5c10a07982491369ba9db9b5/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
  </body>
</html>