<?php
$message = [];
  if ($_SERVER["REQUEST_METHOD"] == "POST") { 
    require_once ('../../dbconnection/connection.php');
    $title = $_POST['title'];
    $content = $_POST['content'];
    $sql = "INSERT INTO participation (title, content, create_date)
    VALUES ($title, $content)";

    if ($conn->query($sql) === TRUE) {
        $message['type'] = "success";
        $message['data'] = "New record created successfully";
      } else {
        $message['type'] = "danger";
        $message['data'] = "Error: " . $sql . "<br>" . $conn->error;
      }

        $conn->close();

  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Add new page</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="../images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="../vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="../vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../css/util.css">
	<link rel="stylesheet" type="text/css" href="../css/main.css">
<!--===============================================================================================-->
</head>
<body> 
  <div class="container">
    <h2>Add New page</h2>
    <?php if(empty($message)): ?>
      <div class="alert alter-<?php echo $message['type'] ?>" role="alert">
          <?php echo $message ?>
      </div>
    <?php endif ?>
    <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
      <div class="form-group">
        <label for="title">tile</label>
        <input type="text" name="title" class="form-control">
       
      </div>
      <div class="form-group">
        <label for="content">content</label>
        <textarea name="content" class="form-control"></textarea>
      </div>
      <div class="form-check">
        <input type="checkbox" class="form-check-input" id="exampleCheck1">
        <label class="form-check-label" for="exampleCheck1">Check me out</label>
      </div>
      <button type="submit" class="btn btn-primary">add</button>
    </form>
</div>
</body>
</html>
