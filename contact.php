
<section class="contact-form" id="contact" style="padding:42px;">
  <div class="container">
    <div class="row">
      <h3 data-aos="zoom-out" class="portfolio-title aos-init aos-animate">Contact <span>Me</span></h3>
      <iv class="contact-info">
        <div class="col-md-12">
          <div class="col-md-4 address">
            <ul class="list-unstyled">
              <li class="txt1"> <i class="fa fa-map-marker p-right" aria-hidden="true"></i> Address</li>
              <li class="txt2">Kathmandu, Nepal </li>
            </ul>
          </div>
          <div class="col-md-4 phone">
            <ul class="list-unstyled">
              <li class="txt1"> <i class="fa fa-phone p-right" aria-hidden="true"></i> Lets Talk</li>
               <li class="txt2"> <a href="#"> Skype ID: anzu.gurung1 </a></li>
<!--               <li class="txt2"> <a href="tel:9825488929"> Phone no: +977-9825488929 </a></li> -->
            </ul>
          </div>
          <div class="col-md-4 email">
            <ul class="list-unstyled">
              <li class="txt1"> <i class="fa fa-envelope-o p-right" aria-hidden="true"></i> Email</li>
              <li class="txt2"><a href="mailto:anj00gur00ng@gmail.com"> grg.puru@gmail.com</a> </li>
          </div>
         
        </div>
      </div>
      <!-- <div class="col-md-6">
        <div class="card">
          <div class="card-body">
            <form>
              <div class="form-group">
                <input id="Full Name" name="Full Name" placeholder="Full Name" class="form-control" type="text">
              </div>
              <div class="form-group">
                <input type="email" class="form-control" placeholder="Email">
              </div>
              <div class="form-group">
                <input id="Mobile No." name="Mobile No." placeholder="Mobile No." class="form-control" required="required" type="text">
              </div>
              <div class="form-group">
                <textarea class="form-control message-box" name="" rows="6" placeholder="Message"></textarea>
              </div>
              <div class="form-group">
                <a href="" class="send-button"> Send Message</a>
              </div>
            </form>
          </div>
        </div>
      </div> -->
    </div>
  </div>
</section>